## What it is ##

A _herokuish_ buildpack to run dotnet tests **inside** the Gitlab CI/CD.

The buildpack determines the dotnet version from the **\<TargetFramework\>** segment from the first *.csproj file that does not contains "Tests.csproj".

If no tests are present, it just skip testing.
